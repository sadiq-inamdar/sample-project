#! /bin/bash

DIR=".git"
if [ -d "$DIR" ] 
    then
    if [ $# != 1 ]
        then
            echo "Kindly enter destination (prod/test)"
            exit
    else
        JQ=$(jq -V)
        JQ_CHECK=$?
        if [ $JQ_CHECK -ne 0  ]
            then
                echo "JQ package not found, Kindly install jq using your package manager"
                exit
        else
            DEST=$1
            BRANCH=$(git symbolic-ref --short HEAD)
            PROJECT_ID=22306146
            if [ $DEST == "prod" ]
                then
                    echo "=======DEPLOYING TO PRODUCTION======="
                    if [ $BRANCH == "master" ];
                        then
                            URL="intelights.anukai.com"
                            echo "======= $BRANCH IS BEING DEPLOYED TO $URL ======="
                            curl --request POST --header "PRIVATE-TOKEN: $GPAT" --header "Content-Type: application/json" --data '{ "ref": "'"$BRANCH"'", "variables": [ {"key": "DEST", "value": "'"$DEST"'"},{"key": "URL", "value": "'"$URL"'"},{"key": "PROJECT_ID", "value": "'"$PROJECT_ID"'"} ] }' "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipeline" | jq '.'
                    else
                        echo "Error: Cannot push to Produciton. Use only master branch"
                    fi
            elif [ $DEST == "test" ]
                then
                    echo "=======DEPLOYING TO TESTING======="
                    URL="test.intelights.anukai.com"
                    echo "======= $BRANCH IS BEING DEPLOYED TO $URL ======="
                    curl --request POST --header "PRIVATE-TOKEN: $GPAT" --header "Content-Type: application/json" --data '{ "ref": "'"$BRANCH"'", "variables": [ {"key": "DEST", "value": "'"$DEST"'"}, {"key": "URL", "value": "'"$URL"'"},{"key": "PROJECT_ID", "value": "'"$PROJECT_ID"'"} ] }' "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipeline" | jq '.'
            else
                echo "Kindly enter destination (prod/test)"
                exit
            fi
        fi
    fi
else
    echo "Error: ${DIR} not found. Can not continue."
fi

